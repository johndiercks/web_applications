<?php 
if ( ! defined( 'ABSPATH' ) ) exit;

include 'config.php';
include 'ssm_cs_post_type.php';
include 'ssm_cs_scripts.php';
include 'ssm_meta_boxes.php';
include 'ssm_menu_pages.php';
include 'ssm_wp_ajax.php';
include 'ssm_wp_widgets.php';
//include 'Ask-Rev.php';
include 'admin-class.php';




$MSFSMAdminClass = new MSFSM_AdminClass();


function msf_check_is_subscribe_me_forms_post_type_page() {
	$screen_id = get_current_screen();
	if ($screen_id->id == "edit-subscribe_me_forms" || $screen_id->id == 'subscribe_me_forms') {
		echo "<div class='update notice notice-warning is-dismissible'>
        <p style='font-size:17px;'> <i>Notice :</i> Please save all your subscribers data from old forms and create new forms using our new form builder from  <a href=".admin_url('edit.php?post_type=pluginops_forms')."> Campaigns menu here</a>, In next update these old forms will be removed. </p>
    </div>";
	}
}
add_action( 'admin_notices', 'msf_check_is_subscribe_me_forms_post_type_page' );

  ?>